#!/bin/bash

# Simple script to grab neovim-config from gitlab

cd ~/

# Prompt for confirmation - remove any old configs
read -p "This process will remove any existing neovim configuration? Continue? (y/n) " answer
if [[ $answer =~ ^[Yy]$ ]]; then
    rm -rf .neovim-config
    rm -rf .config/nvim
    rm -rf .local/share/nvim
else
    echo "Aborting..."
    exit 1
fi

# Download and extract our neovim config
curl -sL https://gitlab.uvm.edu/tsbartle/neovim-config/-/archive/main/neovim-config-main.tar.gz | tar xz

# Change into our config directory
mv neovim-config-main .neovim-config
cd .neovim-config

# Run the install script
./install.sh

# Clear out our screen 
clear

