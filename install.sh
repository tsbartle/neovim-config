#!/bin/bash

read -p "Copy the ItsBorkedAgain nvim config to .config/nvim ? (Y/y)" nvimanswer
if [[ $nvimanswer =~ ^[Yy]$ ]]; then
    ln -s $HOME/.neovim-config/nvim $HOME/.config/nvim
else
    echo "Aborting..."
fi

read -p "Install packer.nvim to ~/.local/share ? (Y/y)" answer
if [[ $answer =~ ^[Yy]$ ]]; then
    git clone --depth 1 https://github.com/wbthomason/packer.nvim\
    ~/.local/share/nvim/site/pack/packer/start/packer.nvim
else
    echo "Aborting..."
fi

clear

echo "Enjoy!"
echo
echo "If you see warnings when launching nvim - run ':PackerSync' then quit & relaunch"
echo "then run ':PackerSync' one more time."
