-- config for undotree
-- key remap to undo
vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)

